FROM golang
ADD . /go/src/gitee.com/wanttobeamaster/etcd
ADD cmd/vendor /go/src/gitee.com/wanttobeamaster/etcd/vendor
RUN go install gitee.com/wanttobeamaster/etcd
EXPOSE 2379 2380
ENTRYPOINT ["etcd"]
