#!/usr/bin/env bash

set -e

if ! [[ "$0" =~ scripts/updatebom.sh ]]; then
	echo "must be run from repository root"
	exit 255
fi

echo "installing 'bill-of-materials.json'"
go get -v -u gitee.com/wanttobeamaster/license-bill-of-materials

echo "generating bill-of-materials.json"
license-bill-of-materials \
    --override-file ./bill-of-materials.override.json \
    gitee.com/wanttobeamaster/etcd gitee.com/wanttobeamaster/etcd/etcdctl > bill-of-materials.json

echo "generated bill-of-materials.json"
